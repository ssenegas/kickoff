package com.senegas.kickoff.pitches;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.utils.Disposable;

public abstract class Pitch implements Disposable {
	
	public enum PitchType {
		CLASSIC, WET, SOGGY, ARTIFICIAL, PLAYERMANAGER
	}
	
	public final static int mapWidthInTiles = 80;
	public final static int mapHeightInTiles = 96;	
	public static final int tileWidthInPixel = 16, tileHeightInPixel = 16;
	
	private TiledMap tiledMap;
	private float friction;
	
	public Pitch(String fileName, float friction) {
		tiledMap = new TmxMapLoader().load(fileName);
		this.friction = friction;
	}
	
	public TiledMap getTiledMap() {
		return tiledMap;
	}
	
	float friction()
	{
		return friction;
	}
	
	@Override
	public void dispose() {
		tiledMap.dispose();		
	}

}
