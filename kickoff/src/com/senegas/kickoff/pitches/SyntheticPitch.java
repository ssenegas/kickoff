package com.senegas.kickoff.pitches;

public class SyntheticPitch extends Pitch {

	public SyntheticPitch() {
		super("pitches/synthetic.tmx", 0.975f );
	}

}
