package com.senegas.kickoff.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.senegas.kickoff.entities.Ball;
import com.senegas.kickoff.entities.Player;
import com.senegas.kickoff.pitches.ClassicPitch;
import com.senegas.kickoff.pitches.Pitch;
import com.senegas.kickoff.pitches.PlayerManagerPitch;
import com.senegas.kickoff.pitches.SoggyPitch;
import com.senegas.kickoff.pitches.SyntheticPitch;
import com.senegas.kickoff.pitches.WetPitch;
import com.senegas.kickoff.utils.OrthoCamController;

public class Match implements Screen {

    private OrthogonalTiledMapRenderer renderer;
    private OrthographicCamera camera;
    private OrthoCamController cameraController;
    private BitmapFont font;
	private SpriteBatch batch;
	//private ClassicPitch pitch;
	//private PlayerManagerPitch pitch;
	//private WetPitch pitch;
	//private SoggyPitch pitch;
	private Pitch pitch;
	//private Ball ball;
	private Player player;
	
	@Override
	public void render(float delta) {
		//handleInput();
        
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);   
        
        camera.position.set(player.getX() + player.getWidth() / 2, player.getY() + player.getHeight() / 2, 0);
        camera.update();
        
        renderer.setView(camera);
		renderer.render();
		
        renderer.getSpriteBatch().begin();
//        Color c= renderer.getSpriteBatch().getColor();
//        renderer.getSpriteBatch().setColor(0, 160, 0, 0);
        player.draw(renderer.getSpriteBatch());
//        renderer.getSpriteBatch().setColor(c);
        renderer.getSpriteBatch().end();
		
		batch.begin();
		font.draw(batch, "FPS: " + Gdx.graphics.getFramesPerSecond(), 10, 20); 
		batch.end();        
	}

	@Override
	public void resize(int width, int height) {
		camera.viewportHeight = height / 2.0f;
		camera.viewportWidth = width / 2.0f;
	}

	@Override
	public void show() {
		
		pitch = new ClassicPitch();
		renderer = new OrthogonalTiledMapRenderer(pitch.getTiledMap());
        
        camera = new OrthographicCamera();                   
        camera.position.set(1280 / 2.0f - 8, 1536 / 2.0f + 8, 0);
        //camera.zoom = 0.5f;
        
        //player = new Player(new Sprite(new Texture("entities/player.png")));
        player = new Player();
        player.setX(1280 / 2.0f - 8);
        player.setY(1536 / 2.0f - 8);
        
        cameraController = new OrthoCamController(camera);
		Gdx.input.setInputProcessor(player);
		
		font = new BitmapFont();
		batch = new SpriteBatch();
	}

	@Override
	public void hide() {
		dispose();
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		pitch.dispose();
		renderer.dispose();
		player.getTexture().dispose();
	}
	
	private void handleInput() {
        if(Gdx.input.isKeyPressed(Input.Keys.A)) {
                camera.zoom += 0.02;
        }
        if(Gdx.input.isKeyPressed(Input.Keys.Q)) {
        	camera.zoom -= 0.02;
        }
        if(Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
                if (camera.position.x > 0 + camera.viewportWidth / 2)
                	camera.translate(-3, 0, 0);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
                if (camera.position.x < 1280 - camera.viewportWidth / 2)
                	camera.translate(3, 0, 0);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
                if (camera.position.y > 0 + camera.viewportHeight / 2)
                	camera.translate(0, -3, 0);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.UP)) {
                if (camera.position.y < 1536 - camera.viewportHeight / 2)
                	camera.translate(0, 3, 0);
        }
}	

}
