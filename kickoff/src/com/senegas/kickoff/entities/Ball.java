package com.senegas.kickoff.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class Ball extends Sprite {

	/** acceleration constant (m/s^2) */
	public static final float gravity = 9.81f;
	/** ball mass (kg)<br>
	 * <a href="http://www.fifa.com/">FIFA.com</a> says: <em>not more than 450 g in weight and not less than 410 g</em>
	 */
	public static final float massInGramms = 0.430f;
	/** air resistance term */
	public static final float drag = 0.350f;
	/** bounce angle factor (must be less that 1) */
	public static final float BOUNCE_SPEED_FACTOR = 0.6f;
	
	private Vector3 velocity = new Vector3();
	private float z;
	private float speed = 0;
	
	/** In order to save calculation time, M/K is precalculated */
	//private static final double	M_K = M/K;
	/** In order to save calculation time, K/M is precalculated */
	private static final float	K_M = drag/massInGramms;
	/** In order to save calculation time, MG/K is precalculated */
	private static final float	MG_K = massInGramms*gravity/drag;		
	
	public Ball() {
		super(new Sprite(new Texture("entities/ball.png")));
	}

	@Override
	public void draw(SpriteBatch spriteBatch) {
		update(Gdx.graphics.getDeltaTime());
		super.draw(spriteBatch);
	}
	
	public void update(float deltaTime) {

		//convert speed from m/s to px/s 1m -> 13.12335958px with a speed of 1 we move 1000px 
		
		velocity.x -= (K_M * velocity.x) * deltaTime;
		velocity.y -= (K_M * velocity.y) * deltaTime;	
		velocity.z -= (K_M * velocity.z + gravity) * deltaTime;
		
		z += velocity.z;
		
//		// Save previous values
//		float xold = getX();
//		float yold = y;
//		
//		
//		vxold = vx;
//		vyold = vy;
//
//		// Update acceleration
//		float ax = -c*vxold^2;
//		float ay = -c*vyold^2 - 9.8;
//
//		// Update velocity
//		vx = vxold + ax*dt;
//		vy = vyold + ay*dt;
//
//		// Update position
//		x = xold + vxold*dt + 0.5*ax*dt^2;
//		y = yold + vyold*dt + 0.5*ay*dt^2;
		
	}
	
	public void move(double speed, double angleDir) {
		// convert degrees to radians
		double radians = Math.toRadians(angleDir);
		
		velocity.x = (float)(speed * Math.sin(radians));
		velocity.y = (float)(-speed * Math.cos(radians));
	}
}

