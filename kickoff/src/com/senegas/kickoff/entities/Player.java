package com.senegas.kickoff.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class Player implements InputProcessor {

	private float x, y;
	private Vector2 velocity = new Vector2();
	private float speed = 250f;
	private Texture texture;
	private TextureRegion region[][];
	private final int WIDTH = 16;
	private final int HEIGHT = 16;
	
	/** Player constant direction */
	public static final int NORTH = 0;
	public static final int NORTH_EAST = 1;
	public static final int EAST = 2;
	public static final int SOUTH_EAST = 3;
	public static final int SOUTH = 4;
	public static final int SOUTH_WEST = 5;
	public static final int WEST = 6;
	public static final int NORTH_WEST = 7;
	public static final int NONE = 8;	
	
	private int running_animation[] = { 0, 3, 2, 1, 1, 2, 3, 4, 7, 6, 5, 5, 6, 7 };
	private int animation_length = 14;
	
	private int direction = NORTH;
	private int running_frame = 0;
	private float running_time = 0.0f;
	private float running_frame_dt = 7 / speed;
	
	public Player() {
		texture = new Texture("entities/style1a.png");
		region = TextureRegion.split(texture, WIDTH, HEIGHT);
	}
	
	public void draw(SpriteBatch spriteBatch) {
		
		float dt = Gdx.graphics.getDeltaTime();
		
		update(dt);
		
		if (direction != NONE) {
		    running_time += dt;
		    running_frame = (int) (running_time / running_frame_dt) % animation_length;
		}
		
		int offset_x = ((running_animation[running_frame] + 8 * direction) % 20);
		int offset_y = ((running_animation[running_frame] + 8 * direction) / 20);
		
		spriteBatch.draw(region[offset_y][offset_x], x, y);
	}
	
	public float getX() {
		return x;
	}
	
	public void setX(float x) {
		this.x = x;
	}
	
	public float getY() {
		return y;
	}
	
	public void setY(float y) {
		this.y = y;
	}
	
	public int getWidth() {
		return WIDTH;
	}
	
	public int getHeight() {
		return HEIGHT;
	}
	
	public void update(float deltaTime) {
		/* adjust diagonal velocity */
		if (velocity.x != 0 && velocity.y != 0) {
			if (velocity.x < 0)
				velocity.x = (float) (-0.5f * Math.sqrt(2*speed*speed));
			else
				velocity.x = (float) (0.5f * Math.sqrt(2*speed*speed));

			if (velocity.y < 0)
				velocity.y = (float) (-0.5f * Math.sqrt(2*speed*speed));
			else
				velocity.y = (float) (0.5f * Math.sqrt(2*speed*speed));
		}

		/* update position */
		setX(getX() + velocity.x * deltaTime);
		setY(getY() + velocity.y * deltaTime);

		if (velocity.y > 0) {
			if (velocity.x < 0) {
				direction = NORTH_WEST;
			} else if (velocity.x > 0) {
				direction = NORTH_EAST;
			} else {
				direction = NORTH;
			}
		} else if (velocity.y < 0) {
			if (velocity.x < 0) {
				direction = SOUTH_WEST;
			} else if (velocity.x > 0) {
				direction = SOUTH_EAST;
			} else {
				direction = SOUTH;
			}
		} else if (velocity.x < 0) {
			direction = WEST;
		} else if (velocity.x > 0) {
			direction = EAST;
		} else {
			direction = NONE;
		}

	}
	
	@Override
    public boolean keyDown(int keycode) {
            switch(keycode) {
            case Keys.LEFT:
                    velocity.x = -speed;
                    break;
            case Keys.RIGHT:
                    velocity.x = speed;
                    break;
            case Keys.UP:
                velocity.y = speed;
                break;
            case Keys.DOWN:
                velocity.y = -speed;
                break;        
            }
            return true;
    }

    @Override
    public boolean keyUp(int keycode) {
            switch(keycode) {
            case Keys.LEFT:
            case Keys.RIGHT:
            	velocity.x = 0;
            	break;
            case Keys.UP:
            case Keys.DOWN:
            	velocity.y = 0;
                    
            }
            
            return true;
    }

    @Override
    public boolean keyTyped(char character) {
            return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
            return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
            return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
            return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
            return false;
    }

    @Override
    public boolean scrolled(int amount) {
            return false;
    }
    
    public Texture getTexture() {
    	return texture;
    }
}
