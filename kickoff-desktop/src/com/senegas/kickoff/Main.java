package com.senegas.kickoff;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "kickoff";
		cfg.useGL20 = true;
		cfg.width = 1280;
		cfg.height = 960;
		
		new LwjglApplication(new KickOff(), cfg);
	}
}
